package stream;



import info.PersonalInfo;
import info.SequencePanel;

import java.net.URISyntaxException;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;



import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;

public class FrameProcessor {
	private static final String TAG = "Clayant::FrameProcessor";

	// the state of processor
	public static final int STATE_FINDING_FACE = 0;
	public static final int STATE_TRACKING_FACE = 1;
	public static final int STATE_TRACKING_FAILED = 2;
	private int PROCESS_STATE = STATE_FINDING_FACE;

	// image for view
	public static final int VIEW_RGBA = 0;
	public static final int VIEW_BACKPROJ = 1;
	public static final int VIEW_ILM_NORM = 2;
	private int FRAME_VIEW = VIEW_RGBA;

	private static final Scalar GREEN = new Scalar(0, 255, 55);
	private static final Scalar RED = new Scalar(0, 0, 255);
	private static final Scalar BLUE = new Scalar(255, 0, 0);
	private static final Scalar PURPLE = new Scalar(255, 0, 127);

	public static final boolean USE_REDUCE_IMAGE = true;
	private static final int NframeToSendImageOnTracking = 5;
	private static final int NframeToCorrectTrackingPosition = 20;

	private Size sizeFrameOriginal;
	private Size sizeHsvReduced = new Size(360, 216);
	private Size sizeGrayReduced = new Size(640, 480);

	// the Android activity as a context
	private Context context;
	private FaceDetector faceDet;
	private FaceTracker faceTracker;
	private CommHandler commHandler;

	// handler for every frame
	private String currentSeqid;
	private Mat hsv = new Mat();
	private Mat mReturn = new Mat();
	private Rect[] facesArray;
	private Rect faceSelection;
	
	private int frameCounter = -1;

    boolean sendQueryEveryFrame = false;


	/**
	 * 
	 * @param context
	 */
	public FrameProcessor(Context context) {
		this.context = context;
		
		// Settings
    	SharedPreferences sharedPref =  PreferenceManager.getDefaultSharedPreferences(context);
    	sendQueryEveryFrame = sharedPref.getBoolean("pref_key_collect_stream", false);

    	// Tools
		faceDet = new FaceDetector(context);
		faceTracker = new FaceTracker(context);
		try {
			commHandler = new CommHandler(context);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG + " Cleror", "Server DOWN?");
		}
		
		faceSelection = new Rect();
	}

	public Mat processFrame(Mat mRgba, Mat mGray) {

		// Prepare Input =================================================
		sizeFrameOriginal = mRgba.size();
		// Imgproc.cvtColor(mRgba, hsv, Imgproc.COLOR_BGR2HSV);
		Imgproc.cvtColor(mRgba, hsv, Imgproc.COLOR_RGB2HSV);

		if (USE_REDUCE_IMAGE) {
			Imgproc.resize(hsv, hsv, sizeHsvReduced);
			Imgproc.resize(mGray, mGray, sizeGrayReduced);
		}else{
			sizeHsvReduced = mRgba.size();
			sizeGrayReduced = mRgba.size();
		}

		// Process frame according to state ==============================
		switch (PROCESS_STATE) {
		case STATE_FINDING_FACE:
			// do face detection
			facesArray = faceDet.detect(mGray);

			// if face is found and face valid, initialize tracker
			// TODO: what is condition of face validity?
			if (facesArray.length > 0) {
				faceSelection = facesArray[0];
				// Core.rectangle(mRgba, faceSelection.tl(), faceSelection.br(), new Scalar(177, 255, 0), 3);
				faceTracker.reset();

				// select only inner portion of the detected face as a basis for tracking (histogram)
				Rect trf = new Rect();
				// using the size of tracker (hsvReduced)
				transformRect(sizeGrayReduced, sizeHsvReduced, faceSelection, trf);
				int x = trf.x + trf.width / 6;
				int y = trf.y + trf.height / 6;
				int w = 2 * trf.width / 3;
				int h = 2 * trf.height / 3;
				Rect fHistBase = new Rect(x, y, w, h);

				// init tracking
				faceTracker.trackFace(hsv, fHistBase);

				// Generate new sequence id for tracking
				currentSeqid = commHandler.generateNewSeqid();
				commHandler.prepareSequence(currentSeqid);
				// Reset frame counter
				this.frameCounter = 0;
				
				if (this.shouldWeSend()) {
					Mat theFace = new Mat(mGray, faceSelection);
					Log.i(TAG + " FaceSize", theFace.size().toString());
					commHandler.queryFace(theFace, currentSeqid);
				}
				this.switchState(STATE_TRACKING_FACE);
			}
			break;

		case STATE_TRACKING_FACE:
			faceTracker.trackFace(hsv, faceSelection);
			Rect trackWindow = faceTracker.trackWindow;
			this.frameCounter++;

			// Check whether the tracker is failed at this point
			if (faceTracker.getState() == FaceTracker.STATE_FAILED) {
				faceTracker.reset();
				if (this.getViewMode() == VIEW_BACKPROJ)
					
					this.switchView(VIEW_RGBA);
				this.switchState(STATE_TRACKING_FAILED);
			} else {
				//
				if (frameCounter % NframeToCorrectTrackingPosition == 0){
					// do face detection
					facesArray = faceDet.detect(mGray);
					if (facesArray.length > 0){
						Rect fr_det = facesArray[0];
						// the detected face rect equivalent in original and tracker frame size
						Rect fr_ori = new Rect(),  fr_tra = new Rect();
						transformRect(sizeGrayReduced, sizeFrameOriginal, fr_det, fr_ori);
						transformRect(sizeGrayReduced, sizeHsvReduced, fr_det, fr_tra);
						// draw the detected face_rect on original frame size
						Core.rectangle(mRgba, fr_ori.br(), fr_ori.tl(), BLUE);
						
						// Check whether the detected face_rect (fr_det) overlap with track window
						// First, convert it to the frame size that is used in tracker
						Rect roi = new Rect();
						if (overlapRoi(fr_tra.tl(), trackWindow.tl(), fr_tra.size(), trackWindow.size(), roi)) {
							// Drawing for the purpose of drawing, in original frame size
							Rect roi_ori = new Rect();
							transformRect(sizeHsvReduced, sizeFrameOriginal, roi, roi_ori);
							Core.rectangle(mRgba, roi_ori.br(), roi_ori.tl(), PURPLE, 3);
							
							// Find intersection area and union area to compute IOU
							double intersectArea = roi.area();
							double unionArea = trackWindow.area() + fr_tra.area() - intersectArea;
							double IOU = intersectArea / unionArea; Log.i(TAG, "IOU=" + IOU);
							// Correct the track window if the intersection more than 50%
							if (IOU > 0.5) trackWindow = faceTracker.trackWindow = fr_tra;
						}
					}	
				}
				
				//TODO: other conditions eg confidence level, number of images, frame number
				// Crop the face and query
				if (this.shouldWeSend()) {
					Rect trw = new Rect();
					transformRect(sizeHsvReduced, sizeGrayReduced, trackWindow, trw);
					Mat trackedFace = new Mat(mGray, trw);
					Log.i(TAG + " FaceSize", trackedFace.size().toString());
					if (trackedFace.height() > trackedFace.width()){
						Size hz = new Size(trackedFace.height(), trackedFace.height());
						int diff = (trackedFace.height() - trackedFace.width())/2;

						Rect roi = new Rect(new Point(diff, 0), trackedFace.size());
						Mat whiteBackground = new Mat(hz, trackedFace.type(), new Scalar(255));
						trackedFace.copyTo(whiteBackground.colRange(diff, diff+trackedFace.width()).rowRange(0,trackedFace.height()));
						trackedFace = whiteBackground;
					}
					
					commHandler.queryFace(trackedFace, currentSeqid);
				}
			}

			break;

		case STATE_TRACKING_FAILED:
			// TODO: handle info etc when state is failed
			this.frameCounter = -1;
			this.currentSeqid = "";
			this.switchState(STATE_FINDING_FACE);
			break;
		}

		// Process frame output for viewing purpose =================================================
		Mat returnFrame = null;
		switch (FRAME_VIEW) {
		case VIEW_RGBA:
			returnFrame = mRgba;
			break;
		case VIEW_BACKPROJ:
			returnFrame = new Mat();
			Imgproc.cvtColor(faceTracker.backproj, returnFrame, Imgproc.COLOR_GRAY2RGB);
			Imgproc.resize(returnFrame, returnFrame, sizeFrameOriginal);
			// returnFrame = faceTracker.backproj;
			break;
		}

		if (PROCESS_STATE == STATE_TRACKING_FACE) {
			// Draw rectangle of tracked face
			Rect trw = faceTracker.trackWindow;
			RotatedRect trb = faceTracker.trackBox;

			if (USE_REDUCE_IMAGE) {
				trw = new Rect();
				trb = new RotatedRect();
				transformRect(sizeHsvReduced, sizeFrameOriginal, faceTracker.trackWindow, trw);
				transformRect(sizeHsvReduced, sizeFrameOriginal, faceTracker.trackBox, trb);
			}
			Core.rectangle(returnFrame, trw.br(), trw.tl(), PURPLE);
			Core.ellipse(returnFrame, trb, GREEN, 2, Core.LINE_AA);
		}

		return returnFrame;
	}


	/**
	 * Investigate various condition before sending a face to server
	 * @return should we send the images or not
	 */
	private boolean shouldWeSend() {
		boolean shouldWe = commHandler.readyToSend();
		boolean thisFrameOK = sendQueryEveryFrame || frameCounter % NframeToSendImageOnTracking == 0;
		shouldWe = shouldWe && thisFrameOK;
		// TODO: whether we have sent enough face image already
		shouldWe = shouldWe && commHandler.letsSendMoreImages(currentSeqid);

		return shouldWe;
	}
	
	public boolean connectionOpen(){
		return commHandler.readyToSend();
	}

	/**
	 * Transform the size and position of a rectangle based on the frame it is in.
	 * Transform means to change the scale of points in the rect based on the frame size.
	 * 
	 * @param srcFrame Current world coordinate
	 * @param dstFrame Target world coordinate
	 * @param srcRect Original rectangle within current world
	 * @param dstRect Target of the rectangle within target world
	 */
	private void transformRect(Size srcFrame, Size dstFrame, Rect srcRect, Rect dstRect) {
		dstRect.height = (int) (srcRect.height * dstFrame.height / srcFrame.height);
		dstRect.width = (int) (srcRect.width * dstFrame.width / srcFrame.width);
		dstRect.y = (int) (srcRect.y * dstFrame.height / srcFrame.height);
		dstRect.x = (int) (srcRect.x * dstFrame.width / srcFrame.width);
	}

	/**
	 * Transform the size and position of a rectangle based on the frame it is in.
	 * Transform means to change the scale of points in the rect based on the frame size.
	 * 
	 * @param srcFrame Current world coordinate
	 * @param dstFrame Target world coordinate
	 * @param srcRect Original rectangle within current world
	 * @param dstRect Target of the rectangle within target world
	 */
	private void transformRect(Size srcFrame, Size dstFrame, RotatedRect srcRect, RotatedRect dstRect) {
		dstRect.size.height = (int) (srcRect.size.height * dstFrame.height / srcFrame.height);
		dstRect.size.width = (int) (srcRect.size.width * dstFrame.width / srcFrame.width);
		dstRect.center.y = (int) (srcRect.center.y * dstFrame.height / srcFrame.height);
		dstRect.center.x = (int) (srcRect.center.x * dstFrame.width / srcFrame.width);
		dstRect.angle = srcRect.angle;
	}	
	
	/**
	 * 
	 * @param tl1
	 * @param tl2
	 * @param sz1
	 * @param sz2
	 * @param roi
	 * @return
	 */
	private boolean overlapRoi(Point tl1, Point tl2, Size sz1, Size sz2, Rect roi) {
		int x_tl = (int) Math.max(tl1.x, tl2.x);
		int y_tl = (int) Math.max(tl1.y, tl2.y);
		int x_br = (int) Math.min(tl1.x + sz1.width, tl2.x + sz2.width);
		int y_br = (int) Math.min(tl1.y + sz1.height, tl2.y + sz2.height);
		if (x_tl < x_br && y_tl < y_br) {
			roi.x = x_tl;
			roi.y = y_tl;
			roi.width = x_br - x_tl;
			roi.height = y_br - y_tl;
			return true;
		}
		return false;
	}


	private void switchState(int targetState) {
		this.PROCESS_STATE = targetState;
	}

	public void switchView(int targetView) {
		this.FRAME_VIEW = targetView;
	}

	public int getViewMode() {
		return this.FRAME_VIEW;
	}
	
	/**
	 * TODO: display info to screen
	 */
	public SequencePanel getCurrentDetectedStudent(){
		return commHandler.getSeqInfos(currentSeqid);
	}
	
	public int getFrameCounter(){
		return frameCounter;
	}
	
	public String getSequenceID(){
		return currentSeqid;
	}
	
	public void askServerToIgnoreStudentId(int id){
		if (getSequenceID() != "" && commHandler.readyToSend())
			commHandler.ignoreStudentID(id, getSequenceID());
	}
	

	public void stopQueryingCurrentSequence(){
		commHandler.stopQueryingForSequence(currentSeqid);
	}

}
