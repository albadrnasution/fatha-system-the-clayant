package stream;


import info.PersonalInfo;
import info.PersonalInfoBank;
import info.SequencePanel;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.java_websocket.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import clayant.Clayant;
import clayant.SessionManager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;

public class CommHandler extends Clayant {
	private static final String TAG = "Clayant::CommHandler";

	// the Android activity as a context
	private Context context;
	private SessionManager _session;
	public PersonalInfoBank bankInfos;

	// Variable handler among function
	private String currentClassroomID;

	Map<String, Boolean> continueQuerying = new HashMap<String, Boolean>();

	public CommHandler(Context context) throws URISyntaxException {
		this.context = context;
		_session = new SessionManager(context);
		bankInfos = new PersonalInfoBank(context);
		
		currentClassroomID = Integer.toString(_session.getActiveClassroom());
		Log.i(TAG, "========= Current Classroom: " + currentClassroomID + "");

		this.connect();
	}

	public String generateNewSeqid() {
		//Long tsLong = System.currentTimeMillis() / 1000 - 1440000000;
		Long tsLong = System.currentTimeMillis() / 1000;
		
		String seqid = _session.getTeacherId() + "." + tsLong.toString() + "." + currentClassroomID;
		Log.i(TAG, "========= New SequenceID is generated: " + seqid + "");
		return seqid;
	}

	public boolean readyToSend() {
		return this.isOpen();
	}

	/**
	 * 
	 * @param face
	 * @param faceCode
	 */
	public void queryFace(Mat face, String faceCode) {
		Mat resizeimage = face;//new Mat();
		//Imgproc.resize(face, resizeimage, new Size(128, 128));

		MatOfByte byteBuffer = new MatOfByte();
		Highgui.imencode(".jpg", resizeimage, byteBuffer);

		JSONObject sendJson = new JSONObject();
		try {
			sendJson.put("source", "device");
			sendJson.put("request", "recognize");
			sendJson.put("class_id", currentClassroomID);
			sendJson.put("faceseqid", faceCode);

			byte[] bytes = byteBuffer.toArray();
			String base64String = Base64.encodeBytes(bytes);
			sendJson.put("info", base64String);

			// send the sendJson JSON
			this.send(sendJson.toString());
			Log.i(TAG + " Query", "Sending... Seqid:" + faceCode + ", classroom:" + currentClassroomID);
			Log.i(TAG + " Query", "Sending... Bytes:" + bytes.length);
			//Log.i(TAG + " Query", "Sending... Base64:" + base64String.length());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void ignoreStudentID(int id, String faceCode){

		JSONObject sendJson = new JSONObject();
		try {
			sendJson.put("source", "device");
			sendJson.put("request", "ignore_stid");
			sendJson.put("class_id", currentClassroomID);
			sendJson.put("faceseqid", faceCode);
			sendJson.put("info", id);

			// send the sendJson JSON
			this.send(sendJson.toString());
			Log.e(TAG + " Ignore", faceCode + " Ignoring Student:" + id + " in classroom:" + currentClassroomID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
    @Override
    public void onMessage(String message) {
    	Log.d(TAG + " JSON", ">> " + message);

		try {
			// Convert message to JSON
			JSONArray js = new JSONArray(message);
			Log.d(TAG, "JS Length=" + js.length());
			// Extract info from JSON Object
			for (int i=0; i < js.length(); ++i){
				int placement = i;
				JSONObject jo = js.getJSONObject(i);
				int studentID = jo.getInt("index");
				String seqid = jo.getString("seqid");
				String pName = jo.getString("name");
				String pComment = jo.getString("comment");
				double pConf = jo.getDouble("confidence");
				
				//Check whether detected student's info is already in the bank
				if (!bankInfos.stExist(currentClassroomID + "-" + studentID)) {
					// kalau belum tanya ke web
			    	Log.i(TAG + " JSON", "New info to inquiry web page! Student:" + studentID);
			    	if (studentID > 0)
			    		new InquiryWebpage().execute(currentClassroomID, "" + studentID, seqid, "", ""+placement);
				}else{
					// kalau udah ada tinggal ambil dari bank
					PersonalInfo persInfo = bankInfos.getInfo(currentClassroomID, studentID);
					bankInfos.addInfo(seqid, persInfo, placement);
				}
				
				bankInfos.updateConfidence(seqid, pConf);
			}
		} catch (JSONException e) {
	    	Log.e(TAG + "Cleror", "error on converting json onMessage Clayant.java ");
			e.printStackTrace();
		}
    }

    public SequencePanel getSeqInfos(final String sequenceID) {
    	SequencePanel seqPanel = bankInfos.getOrderedResultList(sequenceID);
		Log.i(TAG + " BANK", "Students in " + sequenceID + " are " + seqPanel.getStudentListString());
		return seqPanel;
	}
    
    public void prepareSequence(final String sequenceID){
    	 bankInfos.prepareSequence(sequenceID);
    	 continueQuerying.put(sequenceID, true);
    }

	public boolean letsSendMoreImages(String sequenceID){
		return bankInfos.getConfidence(sequenceID) < 5.0 && continueQuerying.get(sequenceID);
	}
	
	public void stopQueryingForSequence(String sequenceID){
		continueQuerying.put(sequenceID, false);
	}
	
	/**
	 * 
	 * @author albadr.ln
	 */
	class InquiryWebpage extends AsyncTask<String, Void, String> {

		private Exception exception;
		private String _classid;
		private String _studentid;
		private String _faceSeqID;
		private String _password;
		private String _placement;

		protected String doInBackground(String... param) {
			Log.d(TAG + " webpage", "InquiryWebpage for std=" + param[1]);
			// with HttpGet
			InputStream content = null;
			_classid = param[0];
			_studentid = param[1];
			_faceSeqID = param[2];
			_password = param[3];
			_placement = param[4];

			try {
				String url = "http://bee00.aisl.cs.tut.ac.jp/web/index.php/student/info/get_json/" + _classid + "/" + _studentid
						+ "?pwd=" + _password;
				HttpPost httpPost = new HttpPost(url);
				HttpClient httpclient = new DefaultHttpClient();
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httpPost);
				content = response.getEntity().getContent();

				// Convert InputStream to String
				BufferedReader rd = new BufferedReader(new InputStreamReader(content), 4096);
				String line;
				StringBuilder sb = new StringBuilder();
				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}
				rd.close();
				String contentAsString = sb.toString();
				Log.i(TAG + " webpage", "InquiryWebpage, response: " + contentAsString);
				return contentAsString;
			} catch (Exception e) {
				Log.e(TAG + " webpage", e.getClass().getName());
				Log.e(TAG + " webpage", e.getMessage());
				// handle the exception !
				exception = e;
			}
			return "";
		}

		protected void onPostExecute(String json) {
			Log.d(TAG + " webpage", "InquiryWebpage onPostExecute");
			// Convert Json-string to Object
			try {
				JSONObject jObj = new JSONObject(json);
				JSONObject info = jObj.getJSONObject("info");

				// add the new info to the bank
				String classID = this._classid;

				PersonalInfo persInfo = new PersonalInfo(context);
				persInfo.fromJsonInfo(info);
				bankInfos.addInfo(classID, _faceSeqID, persInfo, Integer.parseInt(_placement));
		    	Log.d(TAG + " Bank", "Info is added. Seqid: " + _faceSeqID + ", class:" + classID + ", student:" + persInfo.displayName);
			} catch (JSONException e) {
				// Display failure flag
				e.printStackTrace();
			} catch(NumberFormatException nfe) {
				Log.e(TAG, "Could not parse " + nfe);
			} 
		}
	}
}
