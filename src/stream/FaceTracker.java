package stream;

import java.util.Arrays;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;

import android.content.Context;
import android.util.Log;

public class FaceTracker {
    private static final String    TAG = "Clayant/FaceTracker";
    
    // the state of tracker
    public static final int        STATE_NULL       	= 0;
    public static final int        STATE_INITIALIZED    = 11;
    public static final int        STATE_TRACKING    	= 22;
    public static final int        STATE_FAILED    		= 99;
    private int state = STATE_NULL; 
    
	// the Android activity as a context
    private Context context;
    
	// Option for selecting hue in face tracking
	final int vmin = 10, vmax = 250, smin = 30;
    final int hsize = 16;
    //final float hsranges[] = {0,180};
    final MatOfFloat hranges = new MatOfFloat(new float[]{0,180});
    final MatOfFloat vranges = new MatOfFloat(new float[]{0,256});
    final Scalar sc1 = new Scalar(0, smin, Math.min(vmin,vmax));
    final Scalar sc2 = new Scalar(180, 256, Math.max(vmin, vmax));

    // Variable to be used between frame
	Mat 			hist, backproj = new Mat();
	Mat				mask, hue;
	final MatOfInt	ch = new MatOfInt(new int[]{0,0});
	final MatOfInt	cb = new MatOfInt(new int[]{0});
	public Rect 	trackWindow;
	public RotatedRect 	trackBox;

	public FaceTracker(Context context){
        this.context = context;
        mask = new Mat();
        hue = new Mat();
	}

	public void trackFace(Mat hsv, Rect selection){
        
        Core.inRange(hsv, sc1, sc2, mask);
        hue.create(hsv.size(), hsv.depth());
        Core.mixChannels(Arrays.asList(hsv), Arrays.asList(hue), ch);

        if (hist == null)
        {
            Log.d(TAG, "Initializing histogram");
            // create histogram from selection (first-time only)
        	state = STATE_INITIALIZED;
        	this.initHistogram(hue, mask, selection);
            trackWindow = selection;
        }

        Imgproc.calcBackProject(Arrays.asList(hue), cb, hist, backproj, hranges, 1);
        Core.bitwise_and(backproj, mask, backproj);
    	state = STATE_TRACKING;
        this.trackBox = Video.CamShift(backproj, trackWindow, new TermCriteria( TermCriteria.EPS | TermCriteria.MAX_ITER, 10, 1 ));
        
        // Lastly, check whether this tracking failed (face outside frame or too small or etc)
        this.failCheck();
	}
	
	public Mat initHistogram(Mat hue, Mat mask, Rect selection){
        Mat roi = new Mat(hue, selection);
        Mat maskroi = new Mat (mask, selection);
        hist = new Mat();
        Imgproc.calcHist(Arrays.asList(roi), new MatOfInt(new int[]{0}), maskroi, hist, new MatOfInt(new int[]{hsize}), new MatOfFloat(hranges));
        Core.normalize(hist, hist, 0, 255, Core.NORM_MINMAX);
        
        //Kill unused mat
        roi.release(); maskroi.release();
        return hist;
	}
	
	public void failCheck(){
        Mat trackFace = new Mat(backproj, trackWindow);
		//Core.fi
        // Count the number of white matter (max possibility of face) in tracking region
        // First we need to find the histogram of tracking region
        Mat faceHist = new Mat();
        // Separate histogram as N gray values, black - gray - white
        int N_separation = 4;
        Imgproc.calcHist(Arrays.asList(trackFace), cb, new Mat(), faceHist, new MatOfInt(new int[]{N_separation}), vranges);
        // Set the value to max 255
        Core.normalize(faceHist, faceHist, 0, 255,  Core.NORM_MINMAX);
        
        // Take the most white for whiteMatter consideration
        double whiteMatter = faceHist.get(N_separation - 1, 0)[0];
        
        /// Debugger, show the values of backproj histogram
        String fhs="";
        for (int i=0; i < N_separation; i++)
        	fhs += " " + (int) faceHist.get(i,0)[0];
        Log.d(TAG + " WM", "F = " + fhs + ", current white = " + whiteMatter);
        
        // If white less than usual, fail the tracker
        if (whiteMatter < 64)
        	state = STATE_FAILED;
        // Also, if the area is too small, fail the tracker
        if (trackWindow.area() < 100)
        	state = STATE_FAILED;
        
        // Finally, kill unused mat
        trackFace.release(); faceHist.release();
	}
	
	
	/**
	 * 
	 */
	public void reset(){
        // reset state
		this.state = STATE_NULL;
        
		// reset handler
        this.backproj = new Mat();
        this.hist = null;
    	this.trackWindow = null ;
    	this.trackBox = null ;
	}
	
	public int getState(){
		return this.state;
	}

}
