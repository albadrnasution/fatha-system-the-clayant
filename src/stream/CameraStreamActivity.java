package stream;


import info.ClroomAdapter;
import info.ClroomItem;
import info.PersonalInfo;
import info.SequencePanel;
import info.StudentWidget;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import lfd_deprecated.FacePro;


import nativ.DetectionBasedTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.objdetect.CascadeClassifier;

import clayant.SessionManager;
import clayant.SettingsActivity;

import com.facist.clayant.R;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class CameraStreamActivity extends Activity implements CvCameraViewListener2 {

    private static final String    TAG                 = "Clayant::CameraStream";
    private Mat                    mRgba;
    private Mat                    mGray;
    private Mat                    mReturn;
    
    private FrameProcessor		   framePro;

	private MenuItem        mViewMode, mCameraType;
	private TextView        txtClassInfoView;
	private ListView	    pnlStudentInfoView;
	private TextView        txtState;

    private CameraBridgeViewBase   mOpenCvCameraView;
	private SessionManager _session;
	int cameraIndex = Camera.CameraInfo.CAMERA_FACING_FRONT;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
    				framePro = new FrameProcessor(getApplicationContext());

    				mOpenCvCameraView.setCameraIndex(cameraIndex);
                    mOpenCvCameraView.enableView();
    				mOpenCvCameraView.enableFpsMeter();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public CameraStreamActivity() {
    	
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
		
		// Settings
    	SharedPreferences sharedPref =  PreferenceManager.getDefaultSharedPreferences(this);
    	boolean frontCamera = sharedPref.getBoolean("pref_key_front_camera", false);
    	boolean fullScreen = sharedPref.getBoolean("pref_key_fullscreen", false);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Contents and Layout
        setContentView(R.layout.camera_stream_view);
		this.txtState = (TextView) findViewById(R.id.txtState);
		this.txtClassInfoView = (TextView) findViewById(R.id.txtClassInfo);
		this.pnlStudentInfoView = (ListView) findViewById(R.id.panelInfos);
    	
    	// Camera Settings
    	if (frontCamera && frontCameraExist()) cameraIndex = Camera.CameraInfo.CAMERA_FACING_FRONT;
    	else  cameraIndex = Camera.CameraInfo.CAMERA_FACING_BACK;

    	// Setting up camera layout
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.camera_viewport);
        mOpenCvCameraView.setCvCameraViewListener(this);
		mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		
		// Get the session for classroom
		_session = new SessionManager(getApplicationContext());
		
		// Fetch the information of current active classroom
		JSONObject actClass = _session.getActiveClassroomInfo(); 
		if (actClass != null){
			String class_name;
			try {
				class_name = actClass.getString("name");
				String clsInfo = "" + class_name + "";
				// Display info
				this.txtClassInfoView.setText(clsInfo);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    /**
     * =======================================================================
     * =============== SETTINGS           				 =====================
     * =======================================================================
     */

    public boolean frontCameraExist(){
        int numCameras= Camera.getNumberOfCameras();
        for(int i=0;i<numCameras;i++){
            Camera.CameraInfo info = new CameraInfo();
            Camera.getCameraInfo(i, info);
            if(Camera.CameraInfo.CAMERA_FACING_FRONT == info.facing){
                return true;
            }
        }
        return false;
    }

    /**
     * =======================================================================
     * =============== THE MAIN FUNCTION				 =====================
     * =======================================================================
     */
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();
        mReturn = framePro.processFrame(mRgba, mGray);

        // handle info and display
        this.updatePanelInfos();
        
        return mReturn;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
		mCameraType = menu.add("Switch Camera");
		mViewMode = menu.add("View Mode");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        
        if (item == mCameraType) {
			cameraIndex = cameraIndex == Camera.CameraInfo.CAMERA_FACING_FRONT ? Camera.CameraInfo.CAMERA_FACING_BACK
					: Camera.CameraInfo.CAMERA_FACING_FRONT;
			mOpenCvCameraView.disableView();
			mOpenCvCameraView.setCameraIndex(cameraIndex);
			// mOpenCvCameraView.setCvCameraViewListener(this);
			mOpenCvCameraView.enableView();
		}
        else if (item == mViewMode){
        	if (framePro.getViewMode() == FrameProcessor.VIEW_BACKPROJ){
            	framePro.switchView(FrameProcessor.VIEW_RGBA);  
            }else if (framePro.getViewMode() == FrameProcessor.VIEW_RGBA){
            	framePro.switchView(FrameProcessor.VIEW_BACKPROJ);  
        	}
        }
        
        return true;
    }
   

	private void updatePanelInfos(){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				//update if the frame counter is zero
				//it means, new face detected
				if (framePro.getSequenceID() == ""){
					Log.e(TAG + " View", "Tracking Failed, set adapter to null");
					if (pnlStudentInfoView.getAdapter()!=null)
						pnlStudentInfoView.setAdapter(null);
				}
				else if (framePro.getFrameCounter() == 0){
					Log.i(TAG + " View", "Frame 0 detected " +framePro.getSequenceID());

			        SequencePanel adapter = framePro.getCurrentDetectedStudent();
			        pnlStudentInfoView.setAdapter(adapter);
			        // Handle when the classroom item is clicked
			        pnlStudentInfoView.setOnItemClickListener(new OnItemClickListener() {
			            public void onItemClick(AdapterView parent, View v, int position, long id) {
			                //Toast.makeText(getApplicationContext(), "Click on "+position, Toast.LENGTH_SHORT).show();
							Log.i(TAG + " View", "ITEM CLICK" +position);
			            	SequencePanel sp = (SequencePanel)parent.getAdapter();
			                final PersonalInfo item = sp.searchStudent_byPos(position);
			            	framePro.askServerToIgnoreStudentId(item.getStudentID());
			            	sp.removeStudent(position);
			                Toast.makeText(getApplicationContext(), "Student "+item.displayName + " is removed.", Toast.LENGTH_SHORT).show();
			            }
			        });
			        pnlStudentInfoView.setOnItemLongClickListener(new OnItemLongClickListener() {
			            public boolean onItemLongClick(AdapterView parent, View v, int position, long id) {
							Log.i(TAG + " View", "ITEM LONG CLICK" +position);
							framePro.stopQueryingCurrentSequence();

			            	SequencePanel sp = (SequencePanel)parent.getAdapter();
			                final PersonalInfo item = sp.searchStudent_byPos(position);
			            	sp.removeStudentExcept(item.getStudentID());
							
//			            	SequencePanel sp = (SequencePanel)parent.getAdapter();
//			                final PersonalInfo item = sp.searchStudent_byPos(position);
//			            	framePro.askServerToIgnoreStudentId(item.getStudentID());
//			            	sp.removeStudent(position);
			                Toast.makeText(getApplicationContext(), "Student "+item.displayName + " is selected as correct candidate.", Toast.LENGTH_SHORT).show();
							return true;
			            }
					});

			        adapter.notifyDataSetChanged();
			        pnlStudentInfoView.refreshDrawableState();
				}
				
				
				if (framePro.connectionOpen()){
					txtState.setTextColor(Color.BLUE);
					txtState.setText("Connection Open");
				}else{
					txtState.setTextColor(Color.RED);
					txtState.setText("Connection Closed!");
				}
				
			}
		});
	}

}
