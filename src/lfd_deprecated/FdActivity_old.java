package lfd_deprecated;


import info.PersonalInfo;
import info.PersonalInfoBank;
import info.StudentWidget;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Map;

import nativ.BimaIlumi;
import nativ.DetectionBasedTracker;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.java_websocket.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

import clayant.Clayant;
import clayant.SessionManager;

import com.facist.clayant.R;

import android.app.Activity;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FdActivity_old extends Activity implements CvCameraViewListener2 {

	Clayant client;
	BimaIlumi aisl = new BimaIlumi();
	private SessionManager _session;

	private static final String TAG = "FDA::FD";
	private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 55, 255);
	public static final int JAVA_DETECTOR = 0;
	public static final int NATIVE_DETECTOR = 1;
	static {
		if (!OpenCVLoader.initDebug()) {
		    Log.e(TAG +" Cleror", "Opencv eror napa ya");
			// Handle initialization error
		   // System.loadLibrary("opencv_java"); 
		}
	}
	int cameraIndex = Camera.CameraInfo.CAMERA_FACING_FRONT;
	private MenuItem mCameraType;
	private MenuItem mPreviewBimaIN;
	private MenuItem mUseBimaIN;
	private MenuItem mBackProj;

	private Mat mRgba;
	private Mat mGray;
	private DetectionBasedTracker mNativeDetector;

	private int mDetectorType = JAVA_DETECTOR;
	private String[] mDetectorName;

	private float mRelativeFaceSize = 0.2f;
	private int mAbsoluteFaceSize = 0;
	private boolean previewNrmlzedImg = false;
	private boolean useNormalizedFacedect = false;

	private boolean faceRecognized = false, trackingON = false;
	private boolean backprojMode = false;

	private final int SKIP_NFRAME_FRECT = 12;
	private final int SKIP_NFRAME_QUERY = 6;

	long totalProcessTime = 0;
	int _totalFrame = 0;

	private String lastFaceseqID = "";
	Rect selection;
	FacePro fp;
	PersonalInfoBank _bankInfos = new PersonalInfoBank(getApplicationContext());
	String _currentClassroomID;

	private TextView txtClassInfoView;
	private LinearLayout pnlStudentInfoView;
	TextView txtDummy;

	private CameraBridgeViewBase mOpenCvCameraView;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				fp = new FacePro(getApplicationContext());
				fp.initResources();

				mOpenCvCameraView.setCameraIndex(cameraIndex);
				mOpenCvCameraView.enableView();
				mOpenCvCameraView.enableFpsMeter();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	public FdActivity_old() {
		mDetectorName = new String[2];
		mDetectorName[JAVA_DETECTOR] = "Java";
		mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";

		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.camera_stream_view);
		this.txtDummy = (TextView) findViewById(R.id.txtState);
		this.txtClassInfoView = (TextView) findViewById(R.id.txtClassInfo);
		this.pnlStudentInfoView = (LinearLayout) findViewById(R.id.panelInfos);

		mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.camera_viewport);
		mOpenCvCameraView.setCvCameraViewListener(this);
		mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

		_session = new SessionManager(getApplicationContext());
		_currentClassroomID = Integer.toString(_session.getActiveClassroom());
		Log.i(TAG + " Sequence", "Active Classroom=" + _currentClassroomID);

		// Fetch the information of current active classrom
		String classroom_json = _session.getClassroom_json();
		try {
			JSONArray classrooms = new JSONArray(classroom_json);
			for (int i = 0; i < classrooms.length(); ++i) {
				JSONObject classObject = classrooms.getJSONObject(i);
				String class_id = classObject.getString("id");
				// If class_id is the active one
				if (class_id.equals(_currentClassroomID)) {
					String class_name = classObject.getString("name");
					String clsInfo = "[" + class_name + "]";
					// Display info
					this.txtClassInfoView.setText(clsInfo);
				}
			}
		} catch (JSONException e) {
			this.txtClassInfoView.setText("???");
			e.printStackTrace();
		}

		// Try to connect to server
		try {
			client = new Clayant();
			client.connect();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	}

	public void onDestroy() {
		super.onDestroy();
		mOpenCvCameraView.disableView();
	}

	public void onCameraViewStarted(int width, int height) {
		mGray = new Mat();
		mRgba = new Mat();
	}

	public void onCameraViewStopped() {
		mGray.release();
		mRgba.release();
	}

	private void updateTextView(String msg) {
		final String msgFin = msg;
		txtDummy.post(new Runnable() {
			@Override
			public void run() {
				txtDummy.setText(msgFin);
			}
		});
	}
	
	private void updatePanelInfos(final String faceCode){

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				//SparseArray<PersonalInfo> candidates = _bankInfos.getInfo(faceCode);
//				Log.d(TAG + " View", "Jumlah info of " + faceCode +" is " + candidates.size());
//				if (candidates.size() == 0){
//					pnlStudentInfoView.removeAllViews();
//				}
//				else if (candidates.size() != pnlStudentInfoView.getChildCount()) {
//					pnlStudentInfoView.removeAllViews();
//
//					for(int i=0; i < candidates.size(); i++){
//						PersonalInfo student_info = candidates.valueAt(i);
//						StudentWidget sw = new StudentWidget(getApplicationContext(), student_info);
//						pnlStudentInfoView.addView(sw);
//					}
//					for (Map.Entry<Integer, PersonalInfo> cursor : candidates.entrySet()) {
//						StudentWidget sw = new StudentWidget(getApplicationContext(), cursor.getValue());
//						pnlStudentInfoView.addView(sw);
//					}
//				}
			}
		});
	}

	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		long start = System.currentTimeMillis();

		mRgba = inputFrame.rgba();
		mGray = inputFrame.gray();
		Mat hsv = new Mat();
		Imgproc.cvtColor(mRgba, hsv, Imgproc.COLOR_BGR2HSV);
//		//Imgproc.resize(hsv, hsv, new Size(640, 348));
//		//Imgproc.resize(mGray, mGray, new Size(640, 348));

		MatOfRect faces = new MatOfRect();
		Rect[] facesArray = faces.toArray();

		if (!faceRecognized) {
			// Generate id for this sequence
			Long tsLong = System.currentTimeMillis() / 1000;
			lastFaceseqID = _session.getTeacherId() + "." + tsLong.toString();
			Log.i(TAG, "========= Face Sequence ID: " + lastFaceseqID + " =========");

			// Do Face Detection
			faces = fp.detectFace(mGray);
			facesArray = faces.toArray();
			faces.release();

			// If at least one face is detected, proceed
			if (facesArray.length > 0) {
				Log.d(TAG, "Face is found!");
				// Update the activity state
				faceRecognized = true;
				trackingON = true;

				// Select the first face from array
				// TODO: if more than one face detected?
				selection = facesArray[0];
				fp.resetTracker();

				// Draw rect for display purpose
				Core.rectangle(mRgba, selection.br(), selection.tl(), FACE_RECT_COLOR);

				// Send picture to server when face is detected
				// Wait for the result later when tracking
				if (client.isOpen()) {
					Mat theFace = new Mat(inputFrame.gray(), selection);
					queryFace(theFace, lastFaceseqID);
				}
			}
		}

		if (trackingON) {
			Rect fis = new Rect(selection.x + selection.width / 6, selection.y + selection.height / 6, 2 * selection.width / 3, 2 * selection.height / 3);
			fp.trackFace(hsv, fis);
			RotatedRect trackBox = fp.trackBox;
			Rect trackWindow = fp.trackWindow;

			// if tracking is not failed yet, proceed
			if (fp.trackStatus) {

				if (client.isOpen()) {
					// TODO: just a dummy, remove if pnlStudentsInfo already OK
					String msg = checkReceivedMessage(lastFaceseqID);
					updateTextView(msg);

					if (_totalFrame % SKIP_NFRAME_FRECT == 0) {
						faces = fp.detectFace(mGray);
						facesArray = faces.toArray();
						for (Rect frect : facesArray) {
							Core.rectangle(mRgba, frect.br(), frect.tl(), new Scalar(0, 255, 255, 0));
							Rect roi = new Rect();
							if (overlapRoi(frect.tl(), trackWindow.tl(), frect.size(), trackWindow.size(), roi)) {
								Core.rectangle(mRgba, roi.br(), roi.tl(), new Scalar(55, 0, 255, 50), 5);
								double intersectArea = roi.area();
								double unionArea = trackWindow.area() + frect.area() - intersectArea;
								double IOU = intersectArea / unionArea;
								Log.i(TAG, "IOU=" + IOU);
								// Correct the track window
								// if the intersection more than 50%
								if (IOU > 0.5)
									fp.trackWindow = frect;
									trackWindow = fp.trackWindow;
							}
						}
					}
					
//					if (trackWindow.height!=trackWindow.width){
//						int diff = Math.abs(trackWindow.height - trackWindow.width);
//						if (trackWindow.height > trackWindow.width){
//							trackWindow.width = trackWindow.height;
//							trackWindow.x -= diff/2;
//							trackWindow.x = trackWindow.x >= 0 ? trackWindow.x : 0;
//						}
//						
//						if (trackWindow.x + trackWindow.width > mGray.width()){
//							int d = mGray.width() - (trackWindow.x + trackWindow.width);
//							trackWindow.width -= d+1;
//						}
//						
//						if (trackWindow.y + trackWindow.height > mGray.height()){
//							int d = mGray.height() - (trackWindow.y + trackWindow.height);
//							trackWindow.height -= d+1;
//						}
//					}

					// Send picture for more query to this face tracking
					Mat trackedFace = new Mat(inputFrame.gray(), trackWindow);
					if (_totalFrame % SKIP_NFRAME_QUERY == 0)
						queryFace(trackedFace, lastFaceseqID);
				}
			} else {
				// if failed, reset all status
				faceRecognized = false;
				trackingON = false;
				updatePanelInfos("");
			}
		}
//
//		// Process the frame for display
		Mat returnImage = mRgba;
		// Return must have the size of original frame
		if (previewNrmlzedImg) {
			// TODO: THIS IS BROKEN!!
			Imgproc.resize(fp.frameNormalized, returnImage, mRgba.size());
		} else if (backprojMode) {
			Imgproc.cvtColor(fp.getBackProjImage(), returnImage, Imgproc.COLOR_GRAY2RGB);
		}
		if (trackingON) {
		Core.ellipse(returnImage, fp.trackBox, FACE_RECT_COLOR, 2, Core.LINE_AA);
		// Draw rectangle of tracked face
		Core.rectangle(returnImage, fp.trackWindow.br(), fp.trackWindow.tl(), FACE_RECT_COLOR);
		//Core.rectangle(mRgba, selection.br(), selection.tl(), new Scalar(255, 0, 0), 2);
		}
		// Add number of frames since inception
		_totalFrame += 1;

		// For debugging purpose
		long processTime = System.currentTimeMillis() - start;

		//mRgba.release();
		if (_totalFrame % 100 == 0) System.gc();
		mGray.release();
		return returnImage;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "called onCreateOptionsMenu");
		// mItemFace50 = menu.add("Face size 50%");
		// mItemType = menu.add(mDetectorName[mDetectorType]);
		mCameraType = menu.add("Switch Camera").setIcon(android.R.drawable.ic_menu_camera);
		mPreviewBimaIN = menu.add("Preview Bima IN");
		mUseBimaIN = menu.add("Face Detect Ilumi");
		mBackProj = menu.add("Back Proj View");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
		if (item == mCameraType) {
			cameraIndex = cameraIndex == Camera.CameraInfo.CAMERA_FACING_FRONT ? Camera.CameraInfo.CAMERA_FACING_BACK
					: Camera.CameraInfo.CAMERA_FACING_FRONT;
			mOpenCvCameraView.disableView();
			mOpenCvCameraView.setCameraIndex(cameraIndex);
			// mOpenCvCameraView.setCvCameraViewListener(this);
			mOpenCvCameraView.enableView();
		} else if (item == mPreviewBimaIN) {
			previewNrmlzedImg = !previewNrmlzedImg;
		} else if (item == mUseBimaIN) {
			useNormalizedFacedect = !useNormalizedFacedect;
		} else if (item == mBackProj) {
			backprojMode = !backprojMode;

		}

		// if (item == mItemFace50)
		// setMinFaceSize(0.5f);
		// else if (item == mItemType) {
		// int tmpDetectorType = (mDetectorType + 1) % mDetectorName.length;
		// item.setTitle(mDetectorName[tmpDetectorType]);
		// setDetectorType(tmpDetectorType);
		// }

		return true;
	}

	private void setMinFaceSize(float faceSize) {
		mRelativeFaceSize = faceSize;
		mAbsoluteFaceSize = 0;
	}

	private void setDetectorType(int type) {
		if (mDetectorType != type) {
			mDetectorType = type;

			if (type == NATIVE_DETECTOR) {
				Log.i(TAG, "Detection Based Tracker enabled");
				mNativeDetector.start();
			} else {
				Log.i(TAG, "Cascade detector enabled");
				mNativeDetector.stop();
			}
		}
	}

	/**
	 * 
	 * @param face
	 * @param faceCode
	 */
	private void queryFace(Mat face, String faceCode) {
		Mat resizeimage = new Mat();
		Imgproc.resize(face, resizeimage, new Size(128, 128));

		MatOfByte byteBuffer = new MatOfByte();
		Highgui.imencode(".jpg", resizeimage, byteBuffer);

		JSONObject sendJson = new JSONObject();
		try {
			sendJson.put("source", "device");
			sendJson.put("request", "recognize");
			sendJson.put("class_id", _currentClassroomID);
			sendJson.put("faceseqid", faceCode);

			byte[] bytes = byteBuffer.toArray();
			String base64String = Base64.encodeBytes(bytes);
			sendJson.put("info", base64String);

			// send the sendJson JSON
			client.send(sendJson.toString());
			Log.i("Claya Connection", "Sending... Bytes:" + bytes.length);
			Log.i("Claya Connection", "Sending... Base64:" + base64String.length());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// set client state
		client.imageIsReturned = false;
		// client.jsonIsReturned = false;
	}

	private String checkReceivedMessage(final String faceCode) {
		if (client.jsonIsReturned) {
			Log.d(TAG, "JSON RETURNED");
			JSONArray js = client.json_return;

			try {
				JSONObject jo = js.getJSONObject(0);
				int studentID = jo.getInt("index");
				String pName = jo.getString("name");
				String pAff = jo.getString("comment");
				// String pConf = jo.getString("confidence");
				String putText = "" + studentID + ":" + pName + ", " + pAff;

				Log.d("Claya Overlay", putText);

				if (_bankInfos.stExist(_currentClassroomID + "-" + studentID)) {
					// if the current student_id has exist in the bank,
					// put to the text
					PersonalInfo pi = _bankInfos.getInfo(_currentClassroomID, studentID);
					putText = pi.toString();
					// Add to the info holder of current sequence
					//_bankInfos.addInfo(faceCode, pi);
				} else {
					// if not, inquiry to webpage
					new InquiryWebpage().execute(_currentClassroomID, "" + studentID, faceCode, "");
				}

				// Update panel that display student infos
				updatePanelInfos(faceCode);

				return putText;
			} catch (Exception e) {
				Log.e("Cleror", "Error: " + e.getMessage());
			}
		}
		return "<EMPTY>";
	}

	/**
	 * 
	 * @author albadr.ln
	 */
	class InquiryWebpage extends AsyncTask<String, Void, String> {

		private Exception exception;
		private String _classid;
		private String _studentid;
		private String _faceSeqID;
		private String _password;

		protected String doInBackground(String... param) {
			Log.d(TAG + " login", "InquiryWebpage doInBackground");
			// with HttpGet
			InputStream content = null;
			_classid = param[0];
			_studentid = param[1];
			_faceSeqID = param[2];
			_password = param[3];

			try {
				String url = "http://133.15.23.70/web/index.php/student/info/get_json/" + _classid + "/" + _studentid
						+ "?pwd=" + _password;
				HttpPost httpPost = new HttpPost(url);
				HttpClient httpclient = new DefaultHttpClient();
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httpPost);
				content = response.getEntity().getContent();

				// Convert InputStream to String
				BufferedReader rd = new BufferedReader(new InputStreamReader(content), 4096);
				String line;
				StringBuilder sb = new StringBuilder();
				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}
				rd.close();
				String contentAsString = sb.toString();
				Log.i(TAG + " login", "InquiryWebpage, response: " + contentAsString);
				return contentAsString;
			} catch (Exception e) {
				Log.e(TAG + " login", e.getClass().getName());
				Log.e(TAG + " login", e.getMessage());
				// handle the exception !
				exception = e;
			}
			return "";
		}

		protected void onPostExecute(String json) {
			Log.d(TAG + " login", "InquiryWebpage onPostExecute");
			// Convert Json-string to Object
			try {
				JSONObject jObj = new JSONObject(json);
				JSONObject info = jObj.getJSONObject("info");

				// add the new info to the bank
				String classID = this._classid;

				PersonalInfo persInfo = new PersonalInfo(getApplicationContext());
				persInfo.fromJsonInfo(info);
				//...._bankInfos.addInfo(classID, _faceSeqID, persInfo);
			} catch (JSONException e) {
				e.printStackTrace();
				// Display failure flag
			}
		}
	}

	boolean overlapRoi(Point tl1, Point tl2, Size sz1, Size sz2, Rect roi) {
		int x_tl = (int) Math.max(tl1.x, tl2.x);
		int y_tl = (int) Math.max(tl1.y, tl2.y);
		int x_br = (int) Math.min(tl1.x + sz1.width, tl2.x + sz2.width);
		int y_br = (int) Math.min(tl1.y + sz1.height, tl2.y + sz2.height);
		if (x_tl < x_br && y_tl < y_br) {
			roi.x = x_tl;
			roi.y = y_tl;
			roi.width = x_br - x_tl;
			roi.height = y_br - y_tl;
			return true;
		}
		return false;
	}
}
