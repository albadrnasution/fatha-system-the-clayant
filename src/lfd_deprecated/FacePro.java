package lfd_deprecated;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import nativ.BimaIlumi;
import nativ.DetectionBasedTracker;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.video.Video;

import com.facist.clayant.R;

import android.content.Context;
import android.util.Log;

/**
 * Wrapper for Face Detection and Tracking
 * 
 * @author albadr.ln
 *
 */
public class FacePro {
	
    private Context context;
    
	
	public static final int JAVA_DETECTOR = 0;
	public static final int NATIVE_DETECTOR = 1;
	private static final String TAG = "FDA::FD FacePro";
	
	private CascadeClassifier mJavaDetector;
	private CascadeClassifier mBimaDetector;
	private DetectionBasedTracker mNativeDetector;

	private int mDetectorType = NATIVE_DETECTOR;
	private float mRelativeFaceSize = 0.2f;
	private int mAbsoluteFaceSize = 0;
	
	private boolean useNormalizedFacedect = false;
	
	private Size resizedFrameSize = new Size(480, 360);

	// Illumination Normalization class
	BimaIlumi aisl = new BimaIlumi();
	// The last processed frame normalized with BimaIlumi
	public Mat frameNormalized;

	// Option for selecting hue in face tracking
	final int vmin = 10, vmax = 240, smin = 50;
    final int hsize = 16;
    final float hranges[] = {0,180};
    
    // Variable to be used between frame
	Mat hist, backproj = new Mat();
	int totalWhiteMatter = 0, nFrameIsPassed = 0;

	public boolean trackStatus = true;
	public RotatedRect trackBox = new RotatedRect();
	public Rect trackWindow;
	
	/**
	 * 
	 * @param current
	 */
	public FacePro(Context current){
        this.context = current;
	}
	
	public void initResources(){
		Log.i(TAG, "OpenCV loaded successfully");

		// Load native library after(!) OpenCV initialization
		System.loadLibrary("detection_based_tracker");

		File mCascadeFile;
		try {
			/// Load cascade file from application resources
			//InputStream is = getResources().openRawResource(
			//		R.raw.lbpcascade_frontalface);
			InputStream is = context.getResources().openRawResource(R.raw.hc_frontalface_default);

			File cascadeDir = context.getDir("cascade", Context.MODE_PRIVATE);
			//mCascadeFile = new File(cascadeDir,"lbpcascade_frontalface.xml");
			mCascadeFile = new File(cascadeDir, "hc_frontalface_default.xml");
			FileOutputStream os = new FileOutputStream(mCascadeFile);

			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = is.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			is.close();
			os.close();

			mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());

			if (mJavaDetector.empty()) {
				Log.e(TAG, "Failed to load cascade classifier");
				mJavaDetector = null;
			} else
				Log.i(TAG, "Loaded cascade classifier from "+ mCascadeFile.getAbsolutePath());

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Bima Haar Cascase Classifier
			// load cascade file from application resources
			InputStream is_bim = context.getResources().openRawResource(R.raw.hc_fuzzygen);

			cascadeDir = context.getDir("cascade", Context.MODE_PRIVATE);
			mCascadeFile = new File(cascadeDir, "hc_fuzzygen.xml");
			FileOutputStream os_bim = new FileOutputStream(mCascadeFile);

			byte[] bufferBim = new byte[4096];
			int bytesReadBim;
			while ((bytesReadBim = is_bim.read(bufferBim)) != -1) {
				os_bim.write(bufferBim, 0, bytesReadBim);
			}
			is_bim.close();
			os_bim.close();

			mBimaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
			if (mBimaDetector.empty()) {
				Log.e(TAG, "Failed to load cascade classifier");
				mBimaDetector = null;
			} else
				Log.i(TAG, "Loaded cascade classifier from "+ mCascadeFile.getAbsolutePath());

			mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);
			cascadeDir.delete();

		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
		}

	}
	
	/**
	 * Detect face from the frame using setting of this object 
	 * and return all detected face
	 * 
	 * @param frameGray
	 * @return
	 * @TODO NormalizedImage?
	 */
	public MatOfRect detectFace(Mat frameGray){
		if (useNormalizedFacedect) {
			Mat resizedFrame = new Mat();
			// Resize frame because illumination for full size frame very slow
			Imgproc.resize(frameGray, resizedFrame, resizedFrameSize);			
			// Do illumination normalization on current frame
			frameNormalized = aisl.normalize(resizedFrame);
		}
		
		if (mAbsoluteFaceSize == 0) {
			int height = frameGray.rows();
			if (Math.round(height * mRelativeFaceSize) > 0) {
				mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
			}
			mNativeDetector.setMinFaceSize(mAbsoluteFaceSize);
		}

		// Actually detect faces in the frame
		MatOfRect faces = new MatOfRect();
		if (mDetectorType == JAVA_DETECTOR) {
			if (useNormalizedFacedect && mBimaDetector != null) {
				mBimaDetector.detectMultiScale(frameNormalized, faces, 1.1, 2,
						2, new Size(mAbsoluteFaceSize, mAbsoluteFaceSize),
						new Size());
			} else if (!useNormalizedFacedect && mJavaDetector != null)
				mJavaDetector.detectMultiScale(frameGray, faces, 1.1, 2,
						2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
						new Size(mAbsoluteFaceSize, mAbsoluteFaceSize),
						new Size());
		} else if (mDetectorType == NATIVE_DETECTOR) {
			if (mNativeDetector != null)
				mNativeDetector.detect(frameGray, faces);
		} else {
			Log.e(TAG, "Detection method is not selected!");
		}
		
		return faces;
	}
	
	public void initTracker(Mat hsv, Rect selection){
		
	}
	
	public void trackFace(Mat hsv, Rect selection){
    	int _vmin = vmin, _vmax = vmax;
        Scalar sc1 = new Scalar(0, smin, Math.min(_vmin,_vmax));
        Scalar sc2 = new Scalar(180, 256, Math.max(_vmin, _vmax));
        Mat mask = new Mat();
        Core.inRange(hsv, sc1, sc2, mask);
        MatOfInt ch = new MatOfInt(new int[]{0,0});
        Mat hue = new Mat();
        hue.create(hsv.size(), hsv.depth());
        Core.mixChannels(Arrays.asList(hsv), Arrays.asList(hue), ch);
        
        if (hist == null)
        {
        	hist = trackCreateHistogram(hue, mask, selection);
            trackWindow = selection;
            // Reset counter
            totalWhiteMatter = 0;
            nFrameIsPassed = 0;
        }

        Imgproc.calcBackProject(Arrays.asList(hue), new MatOfInt(new int[]{0}), hist, backproj, new MatOfFloat(hranges), 1);
        Core.bitwise_and(backproj, mask, backproj);
        RotatedRect trackBox = Video.CamShift(backproj, trackWindow, new TermCriteria( TermCriteria.EPS | TermCriteria.MAX_ITER, 10, 1 ));
        
        Mat trackFace = new Mat(backproj, trackWindow);
        // Count the number of white matter (max possibility of face) in tracking region
        // First we need to find the histogram of tracking region
        Mat faceHist = new Mat();
        // Separate histogram as two value, black and white
        Imgproc.calcHist(Arrays.asList(trackFace), new MatOfInt(new int[]{0}), new Mat(), faceHist, new MatOfInt(new int[]{8}), new MatOfFloat(new float[]{0, 255}));
        Core.normalize(faceHist, faceHist, 0, 255,  Core.NORM_MINMAX);
        
        /// Debugger
        String fhs="";
        for (int i=0; i < 8; i++){
        	int val = (int)faceHist.get(i,0)[0];
        	fhs += " " + val;
        }
        Log.d(TAG + " WM", "F = " + fhs + ", Total until Now=" + totalWhiteMatter);
        
        double whiteMatter = faceHist.get(2,0)[0]+faceHist.get(3,0)[0] +faceHist.get(4,0)[0]+faceHist.get(5,0)[0]+faceHist.get(6,0)[0]+faceHist.get(7,0)[0] ;
        
        
        // If current white matter is dropped suddenly from average till this time
        if (nFrameIsPassed > 1){
            double avgWM = totalWhiteMatter / nFrameIsPassed;
            Log.d(TAG + " WM", "White Matter=" + whiteMatter + ", AVG="+avgWM);
            // Which is a third of the average
	        if (whiteMatter < avgWM / 3){
	        	// Update status into false to cancel tracking
	        	trackStatus = false;
	        }else{
	            trackStatus = true;
	            this.trackBox = trackBox;
	        }
        }
        // Update number of white matter for next frame
        totalWhiteMatter += whiteMatter;
        nFrameIsPassed++;
	}
	
	public Mat trackCreateHistogram(Mat hue, Mat mask, Rect selection){
        Mat roi = new Mat(hue, selection);
        Mat maskroi = new Mat (mask, selection);
        Mat hist = new Mat();
        
        Imgproc.calcHist(Arrays.asList(roi), new MatOfInt(new int[]{0}), maskroi, hist, new MatOfInt(new int[]{hsize}), new MatOfFloat(hranges));
        Core.normalize(hist, hist, 0, 255, Core.NORM_MINMAX);
        return hist;
	}
	
	public Mat getBackProjImage(){
		return backproj;
	}
	
	public void resetTracker(){
        trackStatus = true;
		backproj = new Mat();
        hist = null;
        // Reset counter
        totalWhiteMatter = 0;
        nFrameIsPassed = 0;
	}
	
	
}
