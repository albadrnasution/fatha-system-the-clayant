package nativ;

import org.opencv.core.Mat;

/* 
 * Bridge in java side for BimaPure (Bima's Illumination Normalization method).
 */
public class BimaIlumi {
	
    private Mat input;
    private Mat output;
	

    public BimaIlumi() {
        //nativeCreateObject();
    }
    
    /**
     * Do illuminaton normalization for inputImage. Method that will be run is native.
     */
    public Mat normalize(Mat inputImage){
    	//BimaIlumi Java Normalize START
    	output = new Mat();
    	nativeNormalize(inputImage.getNativeObjAddr(), output.getNativeObjAddr());
    	//BimaIlumi Java Normalize END
    	return output;
    }

    private native void nativeCreateObject();
    private native void nativeNormalize(long inputImage, long outputImage);
}
