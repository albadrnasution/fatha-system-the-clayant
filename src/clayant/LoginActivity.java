package clayant;


 
 
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facist.clayant.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

 
public class LoginActivity extends Activity {
    // LogCat tag
	private static final String TAG = "Claya";
    private Button btnLogin;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
 
    long start;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        // TODO: Check session and setting, whether teacher is logged in or not
        
        // Get sprite
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
 
        // Session manager
        session = new SessionManager(getApplicationContext());
 
        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
 
        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	start = System.currentTimeMillis();
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();
 
                // Check for empty data in the form
                if (email.trim().length() > 0 && password.trim().length() > 0) {
                    // login user
                    checkLogin(email, password);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
 
    }
 
    /**
     * function to verify login details in mysql db
     * */
    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";
 
        pDialog.setMessage("Logging in ...");
        showDialog();
        
        String email_encrypted = email;
        String password_encrypted = password;
        
        // Do Authentification!
        new DoAuthentification().execute(email_encrypted, password_encrypted);
    }
 
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    class DoAuthentification extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... param) {
        		Log.d(TAG + " login", "doInBackground");
            	// with HttpGet
            	InputStream content = null;
            	try {
                    HttpPost httpPost = new HttpPost("http://133.15.23.70/web/index.php/student/info/teacher_login/");
            		HttpClient httpclient = new DefaultHttpClient();
                    List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
	                //add data to the post method
	                nameValuePairs.add(new BasicNameValuePair("username", param[0]));
	                nameValuePairs.add(new BasicNameValuePair("password", param[1]));
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    // Execute HTTP Post Request
                    HttpResponse response = httpclient.execute(httpPost);
                    content = response.getEntity().getContent();
            		
            		// Convert InputStream to String
                	BufferedReader rd = new BufferedReader(new InputStreamReader(content), 4096);
                	String line;
                	StringBuilder sb =  new StringBuilder();
                	while ((line = rd.readLine()) != null) {
                		sb.append(line);
                	}
                	rd.close();
                	String contentAsString = sb.toString();
                    Log.d(TAG + " login", "doInBackground The response is: " + contentAsString);
                	return contentAsString;
            	} catch (Exception e) {
                    Log.e(TAG + " login", e.getClass().getName());
                    Log.e(TAG + " login", e.getMessage());
            		//handle the exception !
                    exception = e;
            	}
            	return "";
        }

        protected void onPostExecute(String json) {
    		Log.d(TAG + " login", "onPostExecute");
    		
            
        	//Hide logging in dialog
            hideDialog();
            
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });
            
    		if (exception instanceof org.apache.http.conn.HttpHostConnectException){
	            builder.setMessage("Server cannot be accessed when attemped to login. Check your internet connection.").setTitle("Login Failed");
	            AlertDialog dialog = builder.create();
	            dialog.show();
    		}
            
            // Convert Json-string to Object
            try {
				JSONObject jObj = new JSONObject(json);
				int success = jObj.getInt("success");
				
				// Check success tag
				if (success == 1){
					String tag = jObj.getString("tag");
					String uid = jObj.getString("uid");
					JSONObject user = jObj.getJSONObject("user");
					JSONArray classrooms = jObj.getJSONArray("class");
		            Log.d(TAG + " login", "onPostExecute The response tag: " + user.getString("display_name"));
		            
		            //  Save Login-mode and User&Classroom Data
                    session.setLogin(true);
		            session.setLoginData(inputEmail.getText().toString(), user.getString("display_name"), uid);
		            session.setClassroom(classrooms.toString());

		            //  Start Main Activity
		    		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		    		startActivity(intent);
		            
				}else{
					String error_message = jObj.getString("message");
					// Display failure flag
		            builder.setMessage(error_message).setTitle("Login Failed");
		            AlertDialog dialog = builder.create();
		            dialog.show();
				}
			} catch (JSONException e) {
				e.printStackTrace();
				//  Display failure flag
	            builder.setMessage("Something wrong when attemped to login.").setTitle("Login Failed");
	            AlertDialog dialog = builder.create();
	            dialog.show();
			}
        	long processTime = System.currentTimeMillis() - start;
        	Log.i(TAG + "Time ", "Login time " + processTime + " ms");
            
        }
    }
    
}
