package clayant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
	// LogCat tag
	private static String TAG = SessionManager.class.getSimpleName();

	// Shared Preferences
	SharedPreferences pref;

	Editor editor;
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Shared preferences file name
	private static final String PREF_NAME = "ClayantLogin";
	private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void setLogin(boolean isLoggedIn) {

		editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

		// commit changes
		editor.commit();

		Log.d(TAG, "User login session modified!");
	}

	public void setLoginData(String username, String display_name, String uid) {
		editor.putString("username", username);
		editor.putString("display_name", display_name);
		editor.putString("uid", uid);
		// commit changes
		editor.commit();
	}

	public String getLoginDisplayName() {
		return pref.getString("display_name", "");
	}

	public boolean isLoggedIn() {
		return pref.getBoolean(KEY_IS_LOGGEDIN, false);
	}

	public void reset() {
		editor.remove("username");
		editor.remove("display_name");
		editor.remove("uid");
		// commit changes
		editor.commit();
	}

	public void setClassroom(String cls_json) {
		editor.putString("classroom", cls_json);
		// commit changes
		editor.commit();
	}

	public String getClassroom_json() {
		return pref.getString("classroom", "[]");
	}

	public JSONObject getClassroomInfo(String id) {
		// Fetch the information of current active classrom
		String classroom_json = getClassroom_json();
		try {
			JSONArray classrooms = new JSONArray(classroom_json);
			for (int i = 0; i < classrooms.length(); ++i) {
				JSONObject classObject = classrooms.getJSONObject(i);
				String class_id = classObject.getString("id");
				// If class_id is the active one
				if (class_id.equals(id)) {
					return classObject;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONObject getActiveClassroomInfo(){
		return getClassroomInfo(Integer.toString(getActiveClassroom()));
	}

	public void setActiveClassroom(int id) {
		editor.putInt("active", id);
		// commit changes
		editor.commit();
	}

	public int getActiveClassroom() {
		return pref.getInt("active", -1);
	}

	public String getTeacherId() {
		return pref.getString("uid", "x");
	}
}