package clayant;



import com.facist.clayant.R;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        
        if (!frontCameraExist()){
        	getPreferenceScreen().findPreference("pref_key_front_camera").setEnabled(false);
        }
    }
    
    public boolean frontCameraExist(){
        int numCameras= Camera.getNumberOfCameras();
        for(int i=0;i<numCameras;i++){
            Camera.CameraInfo info = new CameraInfo();
            Camera.getCameraInfo(i, info);
            if(Camera.CameraInfo.CAMERA_FACING_FRONT == info.facing){
                return true;
            }
        }
        return false;
    }
}