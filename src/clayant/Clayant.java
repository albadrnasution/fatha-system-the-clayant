package clayant;
import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class Clayant extends WebSocketClient {

    public Clayant(URI serverUri, Draft draft) {
        super(serverUri, draft);
    }

    public Clayant(URI serverURI) {
        super(serverURI);
    }
    
    public Clayant() throws URISyntaxException{
    	//URI campusIP = new URI("ws://133.15.23.194:9002");
    	//URI albadrPC = new URI("ws://133.15.23.70:9002");
    	//URI nonCampusIP = new URI("ws://192.168.179.1:9002");
    	super(new URI("ws://133.15.23.70:9002"), new Draft_10());
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        System.out.println("new connection opened");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        System.out.println("closed with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(String message) {
    	Log.d("JSON", "received message: " + message);

		try {
			json_return = new JSONArray(message);
			jsonIsReturned = true;
		} catch (JSONException e) {
	    	Log.e("Error", "error on converting json onMessage Clayant.java ");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Override
    public void onMessage(java.nio.ByteBuffer bytes){
    	Log.d("JSON", "Received Bytes "+bytes.array().length);
    	//returnedBytes.clear();
    	returnedBytes = bytes;
		imageIsReturned = true;
   }
    
    @Override
    public void onError(Exception ex) {
        System.err.println("an error occured:" + ex);
    }
    
    public void sendMessage(String message){
    	this.send(message);
    }
    
    public boolean imageIsReturned = false;
    public boolean jsonIsReturned = false;

    public JSONArray json_return;
    public java.nio.ByteBuffer returnedBytes = null;

    public static void main(String[] args) throws URISyntaxException {      
        WebSocketClient client = new Clayant(new URI("ws://localhost:8887"), new Draft_10());
        client.connect();
    }
}