package clayant;

import info.ClroomAdapter;
import info.ClroomItem;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.OpenCVLoader;

import stream.CameraStreamActivity;


import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facist.clayant.R;

/**
 * @author albadr.ln
 *
 */
public class MainActivity extends ActionBarActivity {
	private static final String TAG = "Clayant::MainActivity";
	
	public static final String PREFS_NAME = "ClayantSetting";
	public static final String USERS_INFO = "ClayantUserInfo";
	Clayant client;
	String currentAsset = "";
	TextView txtDisplayName;
	long ms;
	JSONArray classrooms;

    private SessionManager session;
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
//	public static class PlaceholderFragment extends Fragment {
//	
//		public PlaceholderFragment() {}
//		
//		@Override
//		public View onCreateView(LayoutInflater inflater, ViewGroup container,
//					Bundle savedInstanceState) {
//			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//			return rootView;
//		}
//	}
	
	static {
		if (!OpenCVLoader.initDebug()) {
			// Handle initialization error
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		System.out.println("Bismillah");
		

        // Session manager
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()){
	        txtDisplayName = (TextView) findViewById(R.id.txtDisplayName);
	        txtDisplayName.setText(session.getLoginDisplayName());
	        
	        try {
		        String classroom_json = session.getClassroom_json();
				classrooms = new JSONArray(classroom_json);
				
				List<ClroomItem> rl = new ArrayList<ClroomItem>();
				
				for (int i=0; i < classrooms.length(); ++i){
					JSONObject classObject = classrooms.getJSONObject(i);
					ClroomItem item = new ClroomItem();
					item.fromJson(classObject);
					if (item.getId() > 0) rl.add(item);
				}
				
		        ClroomAdapter adapter = new ClroomAdapter(this, rl);
		        ListView listView = (ListView) findViewById(R.id.listClroom);
		        listView.setAdapter(adapter);
		        // Handle when the classroom item is clicked
		        listView.setOnItemClickListener(new OnItemClickListener() {
		            public void onItemClick(AdapterView parent, View v, int position, long id) {
		                final ClroomItem item = (ClroomItem) parent.getItemAtPosition(position);
		                setActiveClassroom(item);
		            }
		        });
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	        	Log.e("Claya", "JSON is not good: " + e.getMessage());
			}
	        
        }

	    //TODO: load last setting
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		//String url = settings.getString("a", "");

		if (!OpenCVLoader.initDebug()) {
			// Handle initialization error
		}
		java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
		java.lang.System.setProperty("java.net.preferIPv4Stack", "true");

//		if (savedInstanceState == null) {
//			getSupportFragmentManager().beginTransaction()
//					.add(R.id.container, new PlaceholderFragment()).commit();
//		}

		try {
			client = new Clayant();
			client.connect();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setActiveClassroom(ClroomItem cl){
    	Log.d("Claya", "Actice Classroom Changed to ID:"+cl.getId());
    	session.setActiveClassroom(cl.getId());
    	
    	TextView clName = (TextView) findViewById(R.id.txtBigActName);
    	TextView clCode = (TextView) findViewById(R.id.txtDescCode);
    	TextView clRoom = (TextView) findViewById(R.id.txtDescRoom);
    	clName.setText(cl.getName());
    	clCode.setText("Code: " + cl.getCode());
    	clRoom.setText("Room: " + cl.getRoom());
		
    	RelativeLayout panelAct = (RelativeLayout) findViewById(R.id.pnlActiveClroom);
        ListView listCl = (ListView) findViewById(R.id.listClroom);
    	// set to visible
        panelAct.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(), "Classroom Selected", Toast.LENGTH_SHORT).show();
    	// set to invisible
        listCl.setVisibility(View.GONE);
	}
	
	public void selectActiveClassroom(View v){
    	RelativeLayout panelAct = (RelativeLayout) findViewById(R.id.pnlActiveClroom);
        panelAct.setVisibility(View.GONE);;
        ListView listCl = (ListView) findViewById(R.id.listClroom);
        listCl.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			//TODO: ClassRoom Setting (fetch from web)
			//TODO: Teacher account
			//TODO: Switch Server IP
	        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
	        startActivity(intent);
			return true;
		} else if (id == R.id.action_logout) {
			// Delete session
	        session.setLogin(false);
	        session.reset();
	        
	        // Launching the login activity
	        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
	        startActivity(intent);
	        finish();
			
		}else if (id == R.id.action_detect) {
//			Intent intent = new Intent(this, FdActivity_old.class);
//			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
	
	/** Called when the user touches the button */
	public void startCameraActivity(View view) {
		Intent intent = new Intent(this, CameraStreamActivity.class);
		startActivity(intent);
	}


	@Override
    protected void onStop(){
       super.onStop();

      // Make Editor object to save settings.
      // All objects are from android.context.Context
      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
      SharedPreferences.Editor editor = settings.edit();
      //editor.putBoolean("silentMode", mSilentMode);

      // Commit the edits!
      editor.commit();
	}
}
