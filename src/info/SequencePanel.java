package info;


import java.util.Vector;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class SequencePanel extends SparseArrayAdapter<PersonalInfo> {
	private static final String TAG = "Clayant::Bank";
	
	
	SparseArray<PersonalInfo> studentList;
	Vector<Integer> falseDetStudentID;

	public SequencePanel(Context context) {
		studentList = new SparseArray<PersonalInfo>();
		falseDetStudentID = new Vector<Integer>();
		setData(studentList);
	}
	
	public void addStudent(PersonalInfo info, int placement){
		int iov = searchStudent(info.getStudentID());
		boolean alreadyRemoved = falseDetStudentID.contains(info.getStudentID());
		if (iov < 0 && !alreadyRemoved){
			/* add only if the student not in the list and not already removed*/
			// check whether the position to place already occupied
			if (studentList.get(placement) == null){/*cek di posisi itu ada orang ga*/
				/* if nothing, put in the placement */
				studentList.put(placement, info);
			}else if (placement < 5){
				this.addStudent(info, placement + 1);
				/* get existing one, insert new one, insert the previously existing in next*/
				/*PersonalInfo prev = studentList.get(placement);
				studentList.put(placement, info);
				Log.i(TAG, " Geser prev_id=" + prev.getStudentID() + " " + prev.displayName);
				this.addStudent(prev, placement + 1);*/
			}
			Log.i(TAG, "studentList added student ID:" + info.getStudentID() + " " + info.displayName);
			String msg = "";
			for(int i=0; i < studentList.size(); i++)
				msg += studentList.valueAt(i).getStudentID() + " | " ;
			Log.i(TAG, "studentList size now:" + studentList.size() + "  List: " + msg);

	        this.notifyDataSetChanged();
		}
	}
	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		final PersonalInfo item = getItem(position);
		item.stRemove.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
        		PersonalInfo p = studentList.valueAt(position);
        		studentList.removeAt(position);
                notifyDataSetChanged();
                falseDetStudentID.add(p.getStudentID());
            }
        });
		
		return item;		
	}
	
	public void removeStudent(int position){
		PersonalInfo p = studentList.valueAt(position);
		studentList.removeAt(position);
        this.notifyDataSetChanged();
        falseDetStudentID.add(p.getStudentID());
	}

	public void removeStudentExcept(int StudentId){
		for (int i = studentList.size()-1; i >=0; --i){
			PersonalInfo p = studentList.valueAt(i);
			if (p.getStudentID() != StudentId){
				studentList.removeAt(i);
			}
		}
        this.notifyDataSetChanged();
	}
	
	public PersonalInfo searchStudent_byPos(int position){
		PersonalInfo p = studentList.valueAt(position);
		return p;
	}


	public int searchStudent(int studentID){
		for (int i = 0; i < studentList.size(); ++i){
			PersonalInfo p = studentList.valueAt(i);
			if (p.getStudentID() == studentID){
				int keyAti = studentList.keyAt(i);
				return keyAti;
			}
		}
		return -1;
	}
	
	public SparseArray<PersonalInfo> getStudentList(){
		return studentList;
	}
	
	public String getStudentListString(){
		String msg = "";
		for(int i=0; i < studentList.size(); i++){
			int key = studentList.keyAt(i);
			PersonalInfo val = studentList.valueAt(i);
			msg += "(" + key + ") " + val.displayName + ", ";
		}
		return msg;
	}

}
