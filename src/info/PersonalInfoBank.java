package info;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

public class PersonalInfoBank {
	private static final String TAG = "Clayant::Bank";
	private Context context;

	/**
	 * Bank of student info based on class-studentID key
	 */
	Map<String, Double> confidenceSeq;
	
	/**
	 * Bank of student info based on faceSequenceID key
	 * Map<FaceSeq, ArrayOf(Student) withKey priority on ordered>
	 */
	//Map<String, SparseArray<PersonalInfo>> infos_byFsid;
	Map<String, SequencePanel> infos_byFsid;
	
	/**
	 * Bank of student info based on class-studentID key
	 */
	Map<String, PersonalInfo> infos_byStid;
	
	public PersonalInfoBank(Context context){
		this.context = context;
		infos_byFsid = new HashMap<String, SequencePanel>();
		infos_byStid = new HashMap<String, PersonalInfo>();
		confidenceSeq = new HashMap<String, Double>();
	}
    
    public void prepareSequence(final String faceseqID){
		SequencePanel seqPanel = new SequencePanel(context);
		infos_byFsid.put(faceseqID, seqPanel);
		confidenceSeq.put(faceseqID, 0.0);
		Log.i(TAG, "SeqPanel "+faceseqID+ " dibuat!");
    }
	
	public void addInfo(String classID, String faceseqID, PersonalInfo info, int placement){
		this.addInfo(faceseqID, info, placement);
		String key = classID + "-" + info.getStudentID();
		infos_byStid.put(key, info);
	}
	
	public void addInfo(String faceseqID, PersonalInfo info, int placement){
		if (infos_byFsid.containsKey(faceseqID)){
			infos_byFsid.get(faceseqID).addStudent(info, placement);
		}else{
			Log.e(TAG, "Wait! SeqPanel Baru Dibuat! Harusnya nggak pernah!");
			SequencePanel seqPanel = new SequencePanel(context);
			infos_byFsid.put(faceseqID, seqPanel);
			seqPanel.addStudent(info, placement);
		}
	}
	
	public void getInfo(int classID, int studentID){
		String key = classID + "-" + studentID;
		infos_byStid.get(key);
	}
	
	public PersonalInfo getInfo(String classID, int studentID){
		String key = classID + "-" + studentID;
		return infos_byStid.get(key);
	}
	
	public SequencePanel getOrderedResultList(String faceseqID){
		if (infos_byFsid.containsKey(faceseqID))
			return infos_byFsid.get(faceseqID);
		Log.e(TAG, "Wait! Empty panel is returned!");
		return new SequencePanel(context);
	}
	
	public void updateConfidence(String faceseqID, double confLevel){
		//Log.i(TAG, "Confidence of " + faceseqID + " is " + confLevel);
		confidenceSeq.put(faceseqID, confLevel);
	}
	
	public double getConfidence(String faceseqID){
		if (infos_byFsid.containsKey(faceseqID))
			return confidenceSeq.get(faceseqID);
		return 0.0;
	}
	
	public boolean fsExist(String fsKey){
		return infos_byFsid.containsKey(fsKey);
	}
	
	public boolean stExist(String stKey){
		return infos_byStid.containsKey(stKey);
	}
	
}
