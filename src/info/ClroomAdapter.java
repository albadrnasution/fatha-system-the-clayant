package info;

import java.util.List;

import com.facist.clayant.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

public class ClroomAdapter extends ArrayAdapter<ClroomItem> {
	
	List<ClroomItem> clrooms;

	public ClroomAdapter(Context context, List<ClroomItem> objects) {
		super(context, R.layout.clroom_item, objects);
		this.clrooms = objects;
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		final ClroomItem item = getItemAtPosition(position);
		
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.clroom_item, null);
		TextView tt = (TextView) v.findViewById(R.id.toptext);
		TextView td = (TextView) v.findViewById(R.id.desctext);
		
		tt.setText(item.getName());
		td.setText(item.getDesc());
		return v;
		
	}


	private ClroomItem getItemAtPosition(int position) {
		return clrooms.get(position);
	}
}
