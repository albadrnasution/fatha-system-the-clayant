package info;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ClroomItem {
	int CL_ID;
	String CL_CODE;
	String CL_NAME;
	String CL_ROOM;
	
	public ClroomItem(){}

	public ClroomItem(int id, String code, String name, String room){
		this.CL_ID = id;
		this.CL_CODE = code;
		this.CL_NAME = name;
		this.CL_ROOM = room;
	}
	
	public void fromJson(JSONObject json){
		try{
			String id = json.getString("id");
			String code = json.getString("code");
			String name = json.getString("name");
			String room = json.getString("room");
			
			this.CL_ID = Integer.parseInt(id);
			this.CL_CODE = code;
			this.CL_NAME = name;
			this.CL_ROOM = room;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
        	Log.e("Claya", "JSON is not good: " + e.getMessage());
		} catch (NumberFormatException e){
			this.CL_ID = -1;
		}
		
	}
	
	public int getId(){
		return CL_ID;
	}
	
	public String getName(){
		return CL_NAME;
	}
	
	public String getDesc(){
		return CL_CODE + " | " + CL_ROOM + ""; 
	}
	
	public String getCode(){
		return CL_CODE;
	}
	
	public String getRoom(){
		return CL_ROOM;
	}
	
	public String toString(){
		return CL_NAME;
	}
	
	
}
