package info;

import java.io.InputStream;
import java.net.URL;

import com.facist.clayant.R;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.view.MotionEventCompat;
import android.text.method.HideReturnsTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class StudentWidget extends RelativeLayout {
	private static final String TAG = "Clayant::InfoHandler";
	
	Context context;

	int INVALID_POINTER_ID = 0;
	int defaultLeft;
	int defaultWidth;
	ImageView stPic;
	TextView stName;
	TextView stComment;
	Button stRemove;

	public StudentWidget(Context context) {
		super(context);
		this.context = context;
        Log.d(TAG, "New StudentWidget is created");

		inflate(context, R.layout.student_widget, this);
		
		
		defaultLeft = this.getLeft();
		defaultWidth = this.getWidth();
		
		//setOnTouchListener(touchMe);
		//setOnClickListener(clickMe);
	}
	
	private OnTouchListener touchMe = new OnTouchListener() {
		private float mLastTouchX;
		// The �eactive pointer�f is the one currently moving our object.
		private int mActivePointerId = INVALID_POINTER_ID;
		
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {

		    switch (event.getAction()) {
		    case MotionEvent.ACTION_DOWN: {
		        final int pointerIndex = MotionEventCompat.getActionIndex(event); 
		        final float x = MotionEventCompat.getX(event, pointerIndex); 
		            
		        // Remember where we started (for dragging)
		        mLastTouchX = x;
		        // Save the ID of this pointer (for dragging)
		        mActivePointerId = MotionEventCompat.getPointerId(event, 0);
		        break;
		    }
	        case MotionEvent.ACTION_MOVE:{
	            // Find the index of the active pointer and fetch its position
	            final int pointerIndex = MotionEventCompat.findPointerIndex(event, mActivePointerId);  
	                
	            final float x = MotionEventCompat.getX(event, pointerIndex); 
	            // Calculate the distance moved
	            final float dx = x - mLastTouchX;
	            // move the widget
	            setLeft(getLeft() + (int)dx);
	            Log.d(TAG + " Swipe", "Width:"+getWidth() + " x:"+x+" dx:"+dx+" right:"+getRight() + " left:"+getLeft() + " deflef"+defaultLeft);
		        if (dx > getRight() / 10 && x > getRight() / 3) {
		            Log.d(TAG + " Swipe", " Suicide!");
		        	suicide();
		        };
		        break;
			} 
	        case MotionEvent.ACTION_UP: {
	            setLeft(defaultLeft);
	            Log.d(TAG + " Swipe", " Action Up");
	            mActivePointerId = INVALID_POINTER_ID;
	            break;
	        }
	                
	        case MotionEvent.ACTION_CANCEL: {
	            setLeft(defaultLeft);
	            Log.d(TAG + " Swipe", " Action Cancel");
	            mActivePointerId = INVALID_POINTER_ID;
	            break;
	        }
	        case MotionEvent.ACTION_POINTER_UP: {
	            Log.d(TAG + " Swipe", " Action Pointer Up");
	        	
	            final int pointerIndex = MotionEventCompat.getActionIndex(event); 
	            final int pointerId = MotionEventCompat.getPointerId(event, pointerIndex); 

	            if (pointerId == mActivePointerId) {
	                // This was our active pointer going up. Choose a new
	                // active pointer and adjust accordingly.
	                final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
	                mLastTouchX = MotionEventCompat.getX(event, newPointerIndex); 
	                mActivePointerId = MotionEventCompat.getPointerId(event, newPointerIndex);
	            }
	            break;
	        }
		    }
			return true;
		}
	};

	private OnClickListener clickMe = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
            Toast.makeText(context, "Click on ", Toast.LENGTH_LONG).show();
//			// TODO Different functionality on click
//			// Remove the Panel
//			// Set the student as correct person
//			// Show detail of the student
		}
	};
	
	private void suicide(){
		setVisibility(GONE);
	}
    
	class LoadImageFromWeb extends AsyncTask<String, Void, Drawable> {

	    private Exception exception;

	    protected Drawable doInBackground(String... urls) {
	    	//download the pic
	    	String url = urls[0];
		    try {
		        Log.d(TAG, "Fetching image from " + url);
		        InputStream is = (InputStream) new URL(url).getContent();
		        Drawable d = Drawable.createFromStream(is, "");
		        Log.d(TAG, "Fetching image SUCCESS");
		        return d;
		    } catch (Exception e) {
		        Log.e(TAG, e.toString());
		        return null;
		    }
	    }

	    protected void onPostExecute(Drawable pic) {
	    	//set the downloaded picture to the panel
			stPic.setImageDrawable(pic);
	    }
	}

}
