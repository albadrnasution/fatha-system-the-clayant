package info;

import info.StudentWidget.LoadImageFromWeb;

import org.json.JSONException;
import org.json.JSONObject;

import com.facist.clayant.R;

import android.content.Context;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class PersonalInfo extends StudentWidget {
	public String displayName;
	public String className;
	public String classCode;
	public String comment;
	public int attendance;	
	public String picUrl;
	
	private int studentID;
	private int classID;
	
	
	public long latestUpdate;
	
	public PersonalInfo(Context context){
		super(context);
		latestUpdate = System.currentTimeMillis();
	}
	
	public int getStudentID(){
		return studentID;
	}
	
	public void fromJsonInfo(JSONObject info){
		latestUpdate = System.currentTimeMillis();
		
		try {
			displayName = info.getString("display_name");
			className = info.getString("class_name");
			classCode = info.getString("class_code");
			comment = info.getString("comment");
			attendance =  info.getInt("attendance");
			
			studentID =  info.getInt("id");
			picUrl =  "http://bee00.aisl.cs.tut.ac.jp/web/" + info.getString("picture");
			
			updateView();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateView(){

		stPic = (ImageView) findViewById(R.id.stdPic);
		stName = (TextView) findViewById(R.id.stdName);
		stComment = (TextView) findViewById(R.id.stdComment);
		stRemove = (Button) findViewById(R.id.stRemove);
		
		stName.setText(displayName);
		stComment.setText(comment);
		
		// fetch image and display in the panel
		new LoadImageFromWeb().execute(picUrl);
	}
	
	public String toString(){
		return "(" + studentID + ") " + displayName + " | " + comment + "(" + attendance + "/100)";
	}
	
}
