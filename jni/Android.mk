LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=STATIC
include D:/opencv/OpenCV-2.4.8-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_MODULE     := detection_based_tracker
LOCAL_SRC_FILES  := DetectionBasedTracker_jni.cpp BimaIlumi_jni.cpp BilumiPure.cpp
LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_LDLIBS     += -llog -ldl


include $(BUILD_SHARED_LIBRARY)
