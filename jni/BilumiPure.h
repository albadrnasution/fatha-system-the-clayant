#ifndef BILUMIPURE_H
#define BILUMIPURE_H

#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace cv;
/* 
 * Illumination normalization class with Bima's Algorithm.
 * This pure version did not involve any "float" variable within it.
 */
class BilumiPure {
	static const int wndwSize = 12;
	static const int halfSize = 6;

	//Fuzzy rule of YaleB data training
	int fuzzyRule[25];

	int width, height, step, channels;

	uchar *dataImgNorm,*dataImgLBHE;
	Mat imgSource, imgNormalized;

	struct histeq
	{
		int p[50000];
	};

	struct histeq hist,subblock;

public:
	BilumiPure();
	void setRule();
	Mat normalize (Mat);
	int area() {return width*height;}

private:
	void HistEq(int);
	void initSourceImage(Mat);

	int int_triangle(int,int,int,int);
	int int_fuzzy_a(int,int);
};


#endif
