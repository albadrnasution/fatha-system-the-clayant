#include "BilumiPure.h"

using namespace cv;
using namespace std;


/* 
 * Implementation of illumination normalization class with Bima's Algorithm.
 * This pure version did not involve any "float" variable within it.
 */

BilumiPure::BilumiPure(){
	int YaleB_FuzzyRule [25] = {10,2,57,142,145,1,0,7,137,118,0,0,2,97,148,0,0,0,57,145,20,2,14,0,31};

	for (int i=0;i<25;i++)
		fuzzyRule[i] = YaleB_FuzzyRule[i];
}

void BilumiPure::initSourceImage(Mat src){
	height=src.size().height;
	width=src.size().width;
	step=src.step;
	channels=src.channels();
	imgNormalized = Mat(height,width,CV_8U);
	imgSource = src;
}

Mat BilumiPure::normalize(Mat imgSource){
	initSourceImage(imgSource);

	int **appearance;
	int **color;
	int **datalhe, **filter;

	Mat imgLocalBHE=Mat(height,width,CV_8U);

	dataImgNorm=(uchar *)imgNormalized.data;
	dataImgLBHE=(uchar *)imgLocalBHE.data;

	// Sub-Block Overlap Histogram Equalization
	appearance=new int *[height];
	for(int i=0;i<height;i++)
		appearance[i]=new int [width];

	filter=new int *[height+1];
	for(int i=0;i<=height;i++)
		filter[i]=new int [width+1];

	datalhe=new int *[height];
	for(int i=0;i<height;i++)
		datalhe[i]=new int [width];

	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
			datalhe[i][j]=0;

	// Create spatio-temporal filter
	int I=0;
	for(int i=1;i<=height;i++)
	{
		int J=0;
		for(int j=1;j<=width;j++)
		{
			if(I==0 && J==0 || I==0 && J==(width/halfSize)-1 || I==(height/halfSize)-1 && J==0 || I==(height/halfSize)-1 && J==(width/halfSize)-1)
				filter[i-1][j-1]=1;
			else if(I==0 && J>=1 && J<=(width/halfSize)-1 || J==0 && I>=1 && I<=(height/halfSize)-1 || I==(height/halfSize)-1 && J>=1 && J<=(width/halfSize)-1 || J==(width/halfSize)-1 && I>=1 && I<=(height/halfSize)-1)
				filter[i-1][j-1]=2;
			else
				filter[i-1][j-1]=4;

			if(j%halfSize==0)
				J++;
		}
		if(i%halfSize==0)
			I++;
	}

	// Subblock Histogram Equalization
	for(int i=0;i<height-halfSize;i+=halfSize)
	{
		for(int j=0;j<width-halfSize;j+=halfSize)
		{
			int x=0;
			for(int k=i;k<i+wndwSize;k++)
			{
				int y=0;
				for(int l=j;l<j+wndwSize;l++)
				{
					subblock.p[x*wndwSize+y] = k >= height || l >= width ? 0 : imgSource.at<uchar>(k, l) % 256;
					y++;
				}
				x++;
			}
			HistEq(wndwSize);

			x=0;
			for(int k=i;k<i+wndwSize;k++)
			{
				int y=0;
				for(int l=j;l<j+wndwSize;l++)
				{
					if (k < height && l < width)
						datalhe[k][l]+=hist.p[x*wndwSize+y];
					y++;
				}
				x++;
			}
		}
	}

	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
			dataImgLBHE[i*step+j*channels]=(int)(datalhe[i][j]/filter[i][j]);

	//cvSmooth(lhe,lhe,CV_BLUR,5);
	blur(imgLocalBHE,imgLocalBHE,Size(5,5));

	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			int imgVal = imgSource.at<uchar>(i, j) % 256;
			// Appearance Model
			appearance[i][j]=dataImgLBHE[i*step+j*channels];

			if(appearance[i][j]>255)
				appearance[i][j]=255;

			// Shadow Model
			int luable = 1000 * imgVal >> 8;
			if (luable >= 1000)
				luable = 999;

			// Illumination Normalization
			int int_a = int_fuzzy_a(luable, 1000*appearance[i][j]);
			int int_b = 1.2 * luable * int_a + 1000;
			int colour = ((float)(int_a * 1000 + 1000)/(int_b)) * imgVal;

			if (colour > 255)
				colour = 255;
			else if (colour < 0)
				colour =  0;

			dataImgNorm[i*step+j*channels]=colour;
		}
	}


	for(int i=0;i<height;i++)
		delete[] datalhe[i];
	delete[] datalhe;

	for(int i=0;i<height;i++)
		delete[] filter[i];
	delete[] filter;

	for(int i=0;i<height;i++)
		delete[] appearance[i];
	delete[] appearance;

	return imgNormalized;
}


void BilumiPure::HistEq(int size)
{
	int int_d, int_temp, *int_lut, *int_h;

	int_lut=new int [260];
	int_h=new int [260];

	for(int i=0;i<256;i++){
		int_h[i]=0;
	}
	
	int_d = 1000000 / (size*size);
	for(int i=0;i<size;i++)
		for(int j=0;j<size;j++){
			int_h[subblock.p[i*size+j]] += int_d;
		}

	int_temp=0;
	for(int i=0;i<256;i++)
	{
		int_temp += int_h[i];

		int_lut[i]= (int_temp << 8) + 500;  /// 255 make white black
	}

	for(int i=0;i<size;i++)
		for(int j=0;j<size;j++)
			hist.p[i*size+j]= int_lut[subblock.p[i*size+j]] / 1000000;

	delete[] int_h;
	delete[] int_lut;
}

int BilumiPure::int_triangle(int a, int b, int c, int x)
{
	int d;
	
	if(x>=a && x<b)
		d=1000*(x-a)/(b-a);
	else if(x>=b && x<c)
		d=1000*(c-x)/(c-b);
	else
		d=0;

	return(d);
}

int BilumiPure::int_fuzzy_a(int a,int b)
{
	int s_dmf[100],t_dmf[100],rb[300];
	int fuzz;
	int temp,temp1;
	int rule1,rule2;
	int no_rule;

	no_rule=5;
	rule1=1000 * 1./(no_rule-1);
	rule2=1000 * 255./(no_rule-1);

	// Fuzzyfikasi : Degree of MF Segmentation
	int j=0;
	s_dmf[0]=int_triangle(0,0,rule1,a);
	for(int i=1;i<no_rule-1;i++)
	{
		s_dmf[i]=int_triangle(j*rule1,(j+1)*rule1,(j+2)*rule1,a);
		j++;
	}
	s_dmf[no_rule-1]=int_triangle((no_rule-2)*rule1,1000,1000,a);

	// Fuzzyfikasi : Degree of MF Sigmoid
	j=0;
	t_dmf[0]=int_triangle(0,0,rule2,b);
	for(int i=1;i<no_rule-1;i++)
	{
		t_dmf[i]=int_triangle(j*rule2,(j+1)*rule2,(j+2)*rule2,b);
		j++;
	}
	t_dmf[no_rule-1]=int_triangle((no_rule-2)*rule2,255000,255000,b);

	// Rule Base
	temp=0;
	for(int i=0;i<no_rule;i++)
	{
		for(int j=0;j<no_rule;j++)
		{
			//rb[i*no_rule+j]=minimum(s_dmf[i],t_dmf[j]);
			rb[i*no_rule+j]=s_dmf[i]*t_dmf[j];
			temp+=rb[i*no_rule+j];
		}
	}

	// Output (Defuzzyfikasi)
	temp1=0;
	for(int i=0;i<no_rule;i++)
		for(int j=0;j<no_rule;j++)
			temp1 += rb[i*no_rule+j] * fuzzyRule[i*no_rule+j];

	fuzz = temp != 0 ? temp1 / temp : temp1 * 1000;

	return(fuzz);
}


