#include <BimaIlumi_jni.h>
#include <BilumiPure.h>
#include <opencv2/core/core.hpp>


#include <android/log.h>
#define LOG_TAG "Claya"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;

/*
 * Bridge for BimaPure and Java part with JNI.
 * The algorithm is run on nativeNormalize function.
 */

JNIEXPORT void JNICALL Java_nativ_BimaIlumi_nativeCreateObject
  (JNIEnv *, jobject){}


JNIEXPORT void JNICALL Java_nativ_BimaIlumi_nativeNormalize
  (JNIEnv *, jobject, jlong matInPtr, jlong matOutPtr)
{
    LOGD("nativeNormalize enter");
	BilumiPure aisl;

	Mat* matIn = (Mat*) matInPtr;
	Mat* matOut = (Mat*) matOutPtr;
	LOGD("nativeNormalize Mat(s) initialized");

	//resize(*matIn, in, Size(1024, 768), 1.0, 1.0, INTER_CUBIC); // resize to 1024x768 resolution

	Mat output = aisl.normalize(*matIn);
	*matOut = output;
	//memcpy(matOut->data, matIn->data, matIn->step * matIn->rows);
    LOGD("nativeNormalize exit");
}


